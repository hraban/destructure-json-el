;;; destructure-json.el --- destructure JSON payload -*- lexical-binding: t; -*-

;; Copyright © 2022 Hraban Luyat

;; Author: Hraban Luyat <hraban@0brg.net>
;; Keywords: lisp json
;; Version: 0.0.1

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A DSL to parse and destructure JSON objects.
;;
;; This allows you to write your (JSON handling) code in a structure that
;; resembles the structure of the data it’s handling.
;;
;; This is written for my own use, and the API is not set in stone, at all. I’m
;; releasing this to learn about creating Emacs packages. There is zero
;; intention to keep the API stable. Please pin and/or fork this package if you
;; need it.

;;; Code:

(require 'cl-lib)
(require 'dash)

(defun destructure-json--symbol-name-plist (l)
  "Turn a list of symbols L into a plist with each symbol’s name.

Example:

(destructure-json--symbol-name-plist '(a b c))
=> (\"a\" a \"b\" b \"c\" c)
"
  (--> l
       (mapcar 'symbol-name it)
       (-zip-lists it l)
       (-flatten it)))

(defmacro destructure-json--with-keys (h keys &rest body)
  (declare (indent 1))
  `(-let [(&hash ,@(destructure-json--symbol-name-plist keys)) ,h]
     ,@body))

(defmacro destructure-json--inner (obj &rest dsl)
  "Inner macro for desugaring a parsed JSON object."
  `(let (({} ,obj))
     ,(pcase dsl
        ;; Return the "current" object by default
        ('() '{})
        ;; Explicitly return the current object
        ('({}) '{})
        ;; Descend into object key: / foo ..
        (`(/ ,prop . ,rest)
         `(j (gethash ,(symbol-name prop) {})
            ,@rest))
        ;; Repeat body for every element of array: [] ...
        (`([] . ,rest)
         ;; Reuse {} here because I don’t want to gensym :}
         `(mapcar (lambda ({}) (j {} ,@rest)) {}))
        ;; Array indexing: [3] ...
        (`([,n] . ,rest)
         `(j (elt {} ,n)
            ,@rest))
        ;; Only one item and it’s a list: it’s an expression body
        ((and `(,cmd . ,rest)
              (let (pred listp) cmd))
         (if rest
             `(progn
                ,cmd
                (j {} ,@rest))
           ;; Last command.
           cmd))
        (_ (error "Failed to parse j macro DSL")))))

(defmacro destructure-json (str &rest body)
  "DSL for destructuring JSON objects.

Example:

  (setq jstr \"{\\\"foo\\\": [{\\\"bar\\\": 3}, {\\\"bar\\\": 5}]}\")
  (destructure-json jstr / foo [0] / bar)
  => 3

  (destructure-json jstr / foo [] (message \"item: %s\" (1+ (j {} / bar))))
  -> item: 4
  -> item: 6
  => (\"item: 4\" \"item: 6\")

  (setq jstr \"{\\\"foo\\\": 1, \\\"bar\\\": 2}\")
  (destructure-json jstr (j/with-keys {} (foo bar) (list foo bar)))
  => (1 2)

This API is inspired by the jq tool. There is a “current value” which flows down
the pipeline into every command. Lisp expressions are executed with the ‘{}’
symbol bound to that value.

Operations:

/ keyname :: Assuming the current value is an object: get the value belonging to
key “keyname”.

[] :: Assuming the current value is an array: repeat the entire following
execution path for every single element in the array. This essentially “forks”
parsing.

[n] :: Assuming the current value is an array: get the n-th value from the
array. For example, [3] for the 4-th value.

{} :: The “current” value in the pipeline. This is available in lisp
expressions, too, as a regular variable. Not particularly useful in a DSL
expression, because the “current value” is always implied.

(...) :: A regular Lisp expression, evaluated in the current context of the
pipeline. The ‘{}’ variable can be used to get the current value.

When you’re in a Lisp expression, you can continue with the destructuring
pipeline using the ‘j’ macro (not available outside this macro).

There is an additional utility macro available inside lisp expressions:
‘j/with-keys’. This macro acts like ‘with-slots’, but for the keys of a hash. It
is useful for accessing multiple keys from an object at once.
"
  (declare (indent 1))
  ;; I want a ergonomic alias for ‘destructure-json--inner’ and
  ;; ‘destructure-json--with-keys’, but the best way I know how is to create new
  ;; proxy macros for them. Unfortunately they are not recognized by the editor,
  ;; meaning the (declare (indent 1)) won’t be picked up for these nested
  ;; aliases. 🤷‍♂️
  `(cl-macrolet ((j (&rest r)
                   `(destructure-json--inner ,@r))
                 (j/with-keys (&rest r)
                   `(destructure-json--with-keys ,@r)))
     (j (json-parse-string ,str :array-type 'list) ,@body)))

(provide 'destructure-json)

;;; destructure-json.el ends here
